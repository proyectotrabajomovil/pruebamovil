package com.example.pruebatcnicaduvanmunoz;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.PermissionChecker;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pruebatcnicaduvanmunoz.Model.User;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;

public class RegistroUsuarios extends AppCompatActivity {

    private final int REQUEST_CODE_PERMISSIONS = 1;
    private final int REQUEST_CODE_GALERIA = 2;

    private ImageView fotoPerfil;
    private EditText edNombreRegistro,edCorreoRegistro,edContraseñaRegistro;
    private Button btnRegistrarRegistro;
    private Uri uriImagenGaleria;
    private String name;
    private FirebaseAuth mAuth;
    private StorageReference mStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_usuarios);

        mStorage = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        fotoPerfil = findViewById(R.id.fotoPerfil);
        edNombreRegistro = findViewById(R.id.edNombreRegistro);
        edCorreoRegistro = findViewById(R.id.edCorreoRegistro);
        edContraseñaRegistro = findViewById(R.id.edContraseñaRegistro);
        btnRegistrarRegistro = findViewById(R.id.btnRegistrarRegistro);

        fotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tomarFoto();
            }
        });
        
        btnRegistrarRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrarse();
            }
        });
        
        
        
    }

    private void registrarse() {

        String email = edCorreoRegistro.getText().toString();
        String password = edContraseñaRegistro.getText().toString();
        name = edNombreRegistro.getText().toString();
        if(verificarCampos(email,password,name,uriImagenGaleria)){
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = mAuth.getCurrentUser();
                                StorageReference reference =  mStorage.child("imagenes").child(user.getUid());
                                UploadTask uploadTask = reference.putFile(uriImagenGaleria);
                                obtenerUriImagenStorage(uploadTask,reference,user);




                            } else {
                                Toast.makeText(RegistroUsuarios.this,"No se creo el usuario",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }else{
            Toast.makeText(this, "Campos requeridos", Toast.LENGTH_SHORT).show();
        }
    }

    private void tomarFoto() {
        boolean permisoCamara = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PermissionChecker.PERMISSION_GRANTED;
        if(permisoCamara){
            Intent intent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent,REQUEST_CODE_GALERIA);
        }else{
            ActivityCompat.requestPermissions(this,new String[] {Manifest.permission.CAMERA},REQUEST_CODE_PERMISSIONS);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null) {
            switch (requestCode) {
                case REQUEST_CODE_GALERIA:
                    uriImagenGaleria = data.getData();
                    fotoPerfil.setImageURI(uriImagenGaleria);
            }
        }
    }

    private boolean verificarCampos(String email, String password,String name, Uri uriImagenGaleria) {
        if(email.isEmpty() && password.isEmpty() && name.isEmpty() && uriImagenGaleria==null){
            edCorreoRegistro.setError("Campo requerido");
            edContraseñaRegistro.setError("Campo requerido");
            edNombreRegistro.setError("Campo requerido");
            return false;
        }else if(email.isEmpty()){
            edCorreoRegistro.setError("Campo requerido");
            return false;
        }else if(password.isEmpty()){
            edContraseñaRegistro.setError("Campo requerido");
            return false;
        }else if(name.isEmpty()){
            edNombreRegistro.setError("Campo requerido");
            return false;
        }else if(uriImagenGaleria==null){
            Toast.makeText(this, "Imagen requerida", Toast.LENGTH_SHORT).show();
            return false;
        }else{
            return true;
        }
    }

    private void obtenerUriImagenStorage(UploadTask uploadTask, StorageReference reference, FirebaseUser user){
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    Log.e("ErrorCargaImagen",task.getException().toString());
                    throw task.getException();
                }
                return reference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri uriImagen = task.getResult();
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(name)
                            .setPhotoUri(uriImagen)
                            .build();
                    user.updateProfile(profileUpdates);
                } else {
                    Log.e("ErrorUriImg",task.getException().toString());
                }
            }
        });
    }
}