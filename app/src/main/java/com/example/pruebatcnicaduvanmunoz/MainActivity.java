package com.example.pruebatcnicaduvanmunoz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {


    private EditText edCorreoLogin,edContraseñaLogin;
    private Button btnIngresarLogin,btnRegistrarLogin;
    private TextView txtOlvidoContraseña;
    private ProgressDialog mDialog;

    private FirebaseAuth mAuth;

    private final String TAG = "LoginFirebase";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();

        edCorreoLogin = findViewById(R.id.edCorreoLogin);
        edContraseñaLogin = findViewById(R.id.edContraseñaLogin);
        btnIngresarLogin = findViewById(R.id.btnIngresarLogin);
        btnRegistrarLogin = findViewById(R.id.btnRegistrarLogin);
        txtOlvidoContraseña = findViewById(R.id.txtOlvidoContraseña);
        mDialog = new ProgressDialog(this);


        btnIngresarLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ingresar();
            }
        });

        btnRegistrarLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrar();
            }
        });

        txtOlvidoContraseña.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                olvidoContraseña();
            }
        });
    }

    private void olvidoContraseña() {
        EditText ed = new EditText(this);
        ed.setHint("Ingrese su correo");

        final AlertDialog d = new AlertDialog.Builder(this)
                .setView(ed)
                .setTitle("Olvido contraseña")
                .setMessage("Ingrese su correo")
                .setPositiveButton(android.R.string.ok, null)
                .setNegativeButton(android.R.string.cancel, null)
                .create();
        d.setCanceledOnTouchOutside(false);
        d.setCancelable(false);
        d.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = d.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String email = ed.getText().toString();
                        if(!email.isEmpty()){
                            mAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Toast.makeText(MainActivity.this,"Se envio el correo",Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(MainActivity.this,"No se logro enviar el correo",Toast.LENGTH_LONG).show();
                                    }
                                    mDialog.dismiss();
                                    d.dismiss();
                                }
                            });
                        }else{
                            ed.setError("Campo requerido");
                        }
                    }
                });
            }
        });
        d.show();
    }

    private void registrar() {
        Intent intent = new Intent(this,RegistroUsuarios.class);
        startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser user = mAuth.getCurrentUser();
        ingresarHome(user);
    }

    private void ingresar() {
        String email = edCorreoLogin.getText().toString();
        String password = edContraseñaLogin.getText().toString();

        if(verificarCampos(email,password)){
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                Log.d(TAG, "signInWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                ingresarHome(user);
                            } else {
                                Log.w(TAG, "signInWithEmail:failure", task.getException());
                                Toast.makeText(MainActivity.this, "Error en la autenticacion",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }else {
            Toast.makeText(this, "Error en los campos de texto", Toast.LENGTH_SHORT).show();
        }
    }

    private void ingresarHome(FirebaseUser user) {
        if(user!=null){
            Intent intent = new Intent(this,HomeActivity.class);
            intent.putExtra("name",user.getDisplayName());
            intent.putExtra("urlPhoto",user.getPhotoUrl().toString());
            startActivity(intent);
        }
    }

    private boolean verificarCampos(String email, String password) {
        if(email.isEmpty() && password.isEmpty()){
            edCorreoLogin.setError("Campo requerido");
            edContraseñaLogin.setError("Campo requerido");
            return false;
        }else if(email.isEmpty()){
            edCorreoLogin.setError("Campo requerido");
            return false;
        }else if(password.isEmpty()){
            edContraseñaLogin.setError("Campo requerido");
            return false;
        }else{
            return true;
        }
    }


}