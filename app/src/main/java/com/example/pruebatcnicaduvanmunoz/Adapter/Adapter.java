package com.example.pruebatcnicaduvanmunoz.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.pruebatcnicaduvanmunoz.R;

import com.example.pruebatcnicaduvanmunoz.Model.Usuarios;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    List<Usuarios> usuariosList;

    public Adapter(List<Usuarios> usuariosList) {
        this.usuariosList = usuariosList;
    }

    @NonNull
    @Override
    public Adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter.ViewHolder holder, int position) {
        holder.txtNombreCard.setText("Nombre: "+usuariosList.get(position).getName());
        holder.txtApellidoCard.setText("Apellido: "+usuariosList.get(position).getLastname());
        holder.txtCorreoCard.setText("Correo: "+usuariosList.get(position).getEmail());
        holder.id = usuariosList.get(position).getId();
    }

    @Override
    public int getItemCount() {
        return usuariosList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNombreCard,txtApellidoCard,txtCorreoCard;
        private String id;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtNombreCard = itemView.findViewById(R.id.txtNombreCard);
            txtApellidoCard = itemView.findViewById(R.id.txtApellidoCard);
            txtCorreoCard = itemView.findViewById(R.id.txtCorreoCard);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

        }
    }
}
