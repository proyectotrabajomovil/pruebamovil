package com.example.pruebatcnicaduvanmunoz.Model;

public class User {
    private String photo_url,display_name,email;
    private Boolean active;

    public User() {
    }

    public User(String photo_url, String display_name, String email) {
        this.photo_url = photo_url;
        this.display_name = display_name;
        this.email = email;
        this.active = true;
    }

    public String getPhoto_url() {
        return photo_url;
    }

    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
