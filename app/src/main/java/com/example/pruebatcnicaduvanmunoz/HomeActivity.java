package com.example.pruebatcnicaduvanmunoz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pruebatcnicaduvanmunoz.Adapter.Adapter;
import com.example.pruebatcnicaduvanmunoz.Model.User;
import com.example.pruebatcnicaduvanmunoz.Model.Usuarios;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private ImageView fotoPerfilHome;
    private TextView txtNombreHome;
    private EditText edBusqueda;
    private Button btnBuscar;

    private RecyclerView recyclerUsuarios;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        FirebaseFirestore db = FirebaseFirestore.getInstance();

        fotoPerfilHome = findViewById(R.id.fotoPerfilHome);
        txtNombreHome = findViewById(R.id.txtNombreHome);
        recyclerUsuarios = findViewById(R.id.recyclerUsuarios);
        edBusqueda = findViewById(R.id.edBusqueda);
        btnBuscar = findViewById(R.id.btnBuscar);

        recyclerUsuarios.setLayoutManager(new LinearLayoutManager(this));


        txtNombreHome.setText(getIntent().getStringExtra("name"));
        Picasso.get()
                .load(getIntent().getStringExtra("urlPhoto"))
                .fit()
                .into(fotoPerfilHome);


        db.collection("users")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            ArrayList<Usuarios> usuariosList = new ArrayList<>();
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Usuarios usuarios = document.toObject(Usuarios.class);
                                usuariosList.add(usuarios);
                            }
                            Adapter mascotaAdapter = new Adapter(usuariosList);
                            recyclerUsuarios.setAdapter(mascotaAdapter);
                        } else {
                            Log.d("TAG", "Error getting documents: ", task.getException());
                        }
                    }
                });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buscar();
            }
        });

    }

    private void buscar() {
        String campoBusqueda = edBusqueda.getText().toString();
        if(true){
            FirebaseFirestore db = FirebaseFirestore.getInstance();
            db.collection("users")
                    .whereEqualTo("name",campoBusqueda)
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                ArrayList<Usuarios> usuariosList = new ArrayList<>();
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    Usuarios usuarios = document.toObject(Usuarios.class);
                                    usuariosList.add(usuarios);
                                }
                                if(usuariosList.size()!=0){
                                    Adapter mascotaAdapter = new Adapter(usuariosList);
                                    recyclerUsuarios.setAdapter(mascotaAdapter);
                                }else{
                                    db.collection("users")
                                            .get()
                                            .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                                @Override
                                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                                    if (task.isSuccessful()) {
                                                        ArrayList<Usuarios> usuariosList = new ArrayList<>();
                                                        for (QueryDocumentSnapshot document : task.getResult()) {
                                                            Usuarios usuarios = document.toObject(Usuarios.class);
                                                            usuariosList.add(usuarios);
                                                        }
                                                        Adapter mascotaAdapter = new Adapter(usuariosList);
                                                        recyclerUsuarios.setAdapter(mascotaAdapter);
                                                    } else {
                                                        Log.d("TAG", "Error getting documents: ", task.getException());
                                                    }
                                                }
                                            });
                                    Toast.makeText(HomeActivity.this, "No existe ese usuario", Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Log.d("TAG", "Error getting documents: ", task.getException());
                            }
                        }
                    });


        }else{
            Toast.makeText(this, "no valido", Toast.LENGTH_SHORT).show();
        }
    }



    public static boolean soloLetras(String texto) {
        for (int i=0; i<texto.length();i++)
            if(texto.charAt(i) < 65 || texto.charAt(i) > 90&&texto.charAt(i)<97 || texto.charAt(i)>122) return false;
        return true;
    }

}